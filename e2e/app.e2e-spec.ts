import { App.Evanesa.ComPage } from './app.po';

describe('app.evanesa.com App', () => {
  let page: App.Evanesa.ComPage;

  beforeEach(() => {
    page = new App.Evanesa.ComPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
