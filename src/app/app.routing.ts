import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {MainComponent} from "./main/main.component";
import {DashboardComponent} from "./main/dashboard/dashboard.component";
import {ManagementComponent} from "./main/management/management.component";
import {AuthGuard} from "./auth-guard.service";
import {ManagementUsersComponent} from "./main/management/users/management-users.component";
import {ManagementRolesComponent} from "./main/management/roles/management-roles.component";
import {Role} from "./config/constants/role.enum";
import {CalendarComponent} from "./main/agenda/calendar/calendar.component";
import {AgendaComponent} from "./main/agenda/agenda.component";
import {SegmentComponent} from "./main/agenda/segment/segment.component";

export const mainRoutes:any = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    {
        path: 'main',
        component: MainComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                canActivateChild: [AuthGuard],
                children: [
                    {
                        path: 'dashboard',
                        component: DashboardComponent,
                        icon: 'fa fa-dashboard',
                        displaySub: false,
                        permits: []
                    },
                    {
                        path: 'agenda',
                        component: AgendaComponent,
                        icon: 'fa fa-calendar',
                        displaySub: false,
                        permits: [Role.Administrator, Role.Practicioner, Role.Secretary],
                        children: [
                            { path: '', redirectTo: 'calendar', pathMatch: 'full' },
                            { path: 'calendar', component: CalendarComponent },
                            { path: 'segment', component: SegmentComponent }
                        ]
                    },
                    {
                        path: 'management',
                        component: ManagementComponent,
                        icon: 'fa fa-gears',
                        displaySub: false,
                        permits: [Role.Administrator],
                        children: [
                            { path: '', redirectTo: 'users', pathMatch: 'full' },
                            { path: 'users', component: ManagementUsersComponent },
                            { path: 'roles', component: ManagementRolesComponent }
                        ]
                    },
                    { path: '', redirectTo: 'dashboard', pathMatch: 'full'}
                ]
            },
        ]
    }
    //{ path: '**', component: PageNotFoundComponent }
];

const appRoutes: Routes = mainRoutes;

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
