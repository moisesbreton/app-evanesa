"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
var config_1 = require("../config/config");
var CaptionService = (function () {
    function CaptionService(http) {
        this.http = http;
    }
    CaptionService.prototype.load = function () {
        var _this = this;
        console.log("HERE!");
        return new Promise(function (resolve) {
            _this.http.get(config_1.config.apiURL + "/captions/" + config_1.config.defaultLanguage).map(function (res) { return res.json(); })
                .subscribe(function (captions) {
                if (captions.success) {
                    _this.captions = captions.data;
                }
                resolve();
            });
        });
    };
    CaptionService.prototype.getCaptions = function () {
        return this.captions;
    };
    CaptionService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], CaptionService);
    return CaptionService;
}());
exports.CaptionService = CaptionService;
//# sourceMappingURL=caption.service.js.map