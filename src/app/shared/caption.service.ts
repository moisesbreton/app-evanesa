import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {config} from "../config/config";

@Injectable()
export class CaptionService {
    captions: any = {
        get:null
    };
    constructor(private http: Http) {}

    load() {
        return new Promise((resolve) => {
            this.http.get(config.apiURL + "/captions/language_" + config.defaultLanguage).map(res => res.json())
                .subscribe(captions => {
                    if (captions.success)
                    {
                        this.captions = captions.data;
                    }
                    resolve();
                });
        });
    }

    public getCaptions() {
        this.captions.get = function(cc:string) {
            if (this[cc.toLowerCase()]) {
                return this[cc.toLowerCase()];
            } else {
                return cc;
            }
        };
        return this.captions;
    }
}