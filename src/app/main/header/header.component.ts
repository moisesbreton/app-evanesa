import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {AppService} from "../../app.service";

@Component({
    selector: 'fa-header',
    moduleId: module.id,
    templateUrl: './header.component.html',
    providers: [AppService]
})

export class HeaderComponent implements OnInit {
    @Output() notify: EventEmitter<string> = new EventEmitter<string>();

    config: any;
    userMenu = false;
    user:any;

    constructor(private appService: AppService) {}

    ngOnInit():any {
        this.config = this.appService.getConfig();
        this.user = this.appService.getLoggedUserInfo();
    }

    closeUserMenu() {
        this.userMenu = false;
    }

    doLogout() {
        this.appService.doLogout();
    }

    toggleUserMenu() {
        if (this.userMenu) {
            this.userMenu = false;
        } else {
            this.userMenu = true;
        }
    }

    clickMainMenu() {
        this.notify.emit("clicked");
    }
}
