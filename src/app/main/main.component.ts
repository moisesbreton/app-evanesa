import {Component} from '@angular/core';
import {AppService} from "../app.service";

@Component({
    selector: 'fa-main',
    moduleId: module.id,
    templateUrl: './main.component.html',
    providers: [AppService]
})

export class MainComponent {
    displayMenu:boolean = true;
    hideMenuClass:string = 'hide-left-bar';
    currentMenuClass:string = '';
    showPopUp:boolean = false;

    constructor() {}

    toggleMainMenu(message:string) {
        if (this.displayMenu) {
            this.displayMenu = false;
            this.currentMenuClass = this.hideMenuClass;
        } else {
            this.displayMenu = true;
            this.currentMenuClass = '';
        }
    }
}
