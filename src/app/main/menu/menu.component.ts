 import {Component, OnInit, Input} from '@angular/core';
 import {AppService} from "../../app.service";
 import {Router} from "@angular/router";
 import {mainRoutes} from "../../app.routing";
 import {Role} from "../../config/constants/role.enum"
 import {User} from "../../config/interfaces/user.interface";

@Component({
    selector: 'fa-menu',
    moduleId: module.id,
    templateUrl: './menu.component.html',
    providers: [AppService]
})

export class MenuComponent implements OnInit {
    @Input() displayMenu:string;

    captions: any;
    displayManagementSub:boolean = false;
    activeManagement:boolean = false;
    routeManagement:string = "management";
    main:string = "/main/";
    menuItems = new Array();
    url:string;
    user: User;

    constructor(private appService: AppService, private router: Router) {
    }

    ngOnInit():any {
        this.captions = this.appService.getCaptions();
        this.url = this.router.url;
        this.user = this.appService.getLoggedUserInfo();
        this.router.events.subscribe((val) => this.routerChanged());
        this.setMenu();
    }

    setMenu() {
        this.menuItems = new Array();
        for (let fakeRoutes of mainRoutes) {
            if (fakeRoutes.path == "main") {
                for (let realRoutes of fakeRoutes.children[0].children) {
                    if (realRoutes.path != ""
                        && (realRoutes.permits.length == 0 || realRoutes.permits.indexOf(Number(this.user.role_id)) > -1)) {
                        this.menuItems.push(realRoutes);
                        if (realRoutes.children && this.url.indexOf(realRoutes.path) > 0) {
                            this.toggleManagementSub(realRoutes.path, false, true)
                        } else if (realRoutes.children) {
                            this.toggleManagementSub(realRoutes.path, true)
                        }
                    }
                }
            }
        }
    }

    toggleManagementSub(submenu, forceClose = false, forceOpen = false) {
        for (let item of this.menuItems) {
            if (item.children && item.path == submenu) {
                item.displaySub = !item.displaySub;
                if (forceClose) {
                    item.displaySub = false;
                } else if (forceOpen) {
                    item.displaySub = true;
                }
            } else if (item.children && this.url.indexOf(item.path) < 0) {
                item.displaySub = false;
            }
        }
    }

    routerChanged() {
        this.url = this.router.url;
        this.setMenu();
    }
}
