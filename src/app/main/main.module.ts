import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MainComponent} from './main.component';
import {HeaderComponent} from './header/header.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {MenuComponent} from './menu/menu.component';
import {routing} from '../app.routing';
import {ManagementModule} from './management/management.module';
import {ClickOutsideModule} from 'ng-click-outside';
import {ModalModule} from 'angular2-modal';
import {BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import {FormsModule} from '@angular/forms';
import {AgendaModule} from './agenda/agenda.module';


@NgModule({
    imports:      [
        BrowserModule,
        routing,
        ManagementModule,
        ClickOutsideModule,
        ModalModule.forRoot(),
        BootstrapModalModule,
        FormsModule,
        AgendaModule
    ],
    declarations: [
        MainComponent,
        HeaderComponent,
        DashboardComponent,
        MenuComponent
    ],
    bootstrap:    [ MainComponent ]
})
export class MainModule { }

