import {Component} from '@angular/core';
import {AppService} from "../../app.service";

@Component({
    selector: 'fa-dashboard',
    moduleId: module.id,
    template: '<h1>DASHBOARD</h1>',
    providers: [AppService]
})

export class DashboardComponent {

}