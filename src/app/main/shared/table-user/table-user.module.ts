import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing} from '../../../app.routing';
import {TableUserComponent} from './../table-user/table-user.component';
import {LoadingModule} from '../loading/loading.module';

@NgModule({
    imports:      [
        BrowserModule,
        routing,
        LoadingModule
    ],
    declarations: [
        TableUserComponent
    ],
    exports: [
        TableUserComponent
    ]
})
export class TableUserModule { }

