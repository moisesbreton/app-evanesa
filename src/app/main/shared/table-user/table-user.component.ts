import {Component, OnInit, Input, ViewContainerRef} from '@angular/core';
import {AppService} from '../../../app.service';
import {Overlay, overlayConfigFactory} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {UserFormComponent} from '../../management/users/user-form.component';
import {DeleteUserComponent} from '../../management/users/delete-user.component';
import {ManagementUsersComponent} from '../../management/users/management-users.component';

@Component({
    selector: 'app-table-user',
    moduleId: module.id,
    templateUrl: 'table-user.component.html',
    providers: [AppService]
})

export class TableUserComponent implements OnInit {
    @Input() users: Array<Object>;
    @Input() parent: ManagementUsersComponent;

    captions: any;
    constructor(private appService: AppService, overlay: Overlay, vcRef: ViewContainerRef, public modal: Modal) {
        // overlay.defaultViewContainer = vcRef;
    }

    ngOnInit(): any {
        this.captions = this.appService.getCaptions();
    }

    editUser(user) {
        this.modal.open(UserFormComponent, overlayConfigFactory({ user: user, parent: this.parent }, BSModalContext));
    }

    deactivateUser(user) {
        this.modal.open(DeleteUserComponent, overlayConfigFactory({user: user, parent: this.parent }, BSModalContext));
    }
}
