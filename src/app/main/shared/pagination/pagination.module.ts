import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing} from "../../../app.routing";
import {PaginationComponent} from "./../pagination/pagination.component";

@NgModule({
    imports:      [
        BrowserModule,
        routing
    ],
    declarations: [
        PaginationComponent
    ],
    exports: [
        PaginationComponent
    ]
})
export class PaginationModule { }

