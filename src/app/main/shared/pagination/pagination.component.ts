import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {AppService} from '../../../app.service';
import {Router} from '@angular/router';
import * as _ from 'underscore';

@Component({
    selector: 'app-pagination',
    moduleId: module.id,
    templateUrl: 'pagination.component.html',
    providers: [AppService]
})

export class PaginationComponent implements OnInit, OnChanges {
    @Input() resultTotal:number = 0;
    @Input() maxPerPage:number = 10;
    @Input() page:number = 1;

    captions:any;
    urlPath:string;
    maxPages:number = 0;
    disabledPrevious:boolean = false;
    disabledNext:boolean = false;
    currentInitValue:number = 0;
    currentEndValue:number = 0;
    pages:Array<number>;

    constructor(private appService: AppService, private router: Router) { }

    ngOnInit() {
        this.captions = this.appService.getCaptions();
        this.setPagination();
    }

    ngOnChanges() {
        this.setPagination();
    }

    setPagination() {
        this.urlPath = window.location.pathname;
        this.currentInitValue = ((this.page - 1) * this.maxPerPage) + 1;
        this.currentEndValue = this.page * this.maxPerPage;
        if (this.currentEndValue > this.resultTotal) {
            this.currentEndValue = this.resultTotal;
        }
        this.maxPages = Math.ceil(this.resultTotal/this.maxPerPage);

        this.pages = _.range(1, this.maxPages + 1);

        this.checkActionButtons();
    }

    checkActionButtons() {
        if (this.page == 1 && this.page == this.maxPages) {
            this.disabledPrevious = true;
            this.disabledNext = true;
        } else if (this.page == 1) {
            this.disabledPrevious = true;
            this.disabledNext = false;
        } else if(this.page == this.maxPages) {
            this.disabledNext = true;
            this.disabledPrevious = false;
        } else {
            this.disabledPrevious = false;
            this.disabledNext = false;
        }
    }

    goToPage(page) {
        if (page > 0) {
            this.appService.redirectToURL(this.urlPath + '?page=' + page);
        }
    }

    clickPrevious() {
        let previousPage = this.page - 1;
        if (previousPage > 0) {
            this.appService.redirectToURL(this.urlPath + '?page=' + previousPage);
        }
    }

    clickNext() {
        let nextPage = this.page + 1;
        if (nextPage <= this.maxPages) {
            this.appService.redirectToURL(this.urlPath + '?page=' + nextPage);
        }
    }
}