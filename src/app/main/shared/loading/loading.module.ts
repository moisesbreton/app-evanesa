import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing} from "../../../app.routing";
import {LoadingComponent} from "./loading.component";

@NgModule({
    imports:      [
        BrowserModule,
        routing
    ],
    declarations: [
        LoadingComponent
    ],
    exports: [
        LoadingComponent
    ]
})
export class LoadingModule { }

