import {Component, OnInit, Input} from '@angular/core';
import {AppService} from "../../../app.service";

@Component({
    selector: 'fa-loading',
    moduleId: module.id,
    templateUrl: 'loading.component.html',
    providers: [AppService]
})

export class LoadingComponent implements OnInit {
    @Input() fullInsideElement:boolean = false;
    config: any;

    constructor(private appService: AppService) {}

    ngOnInit(): any {
        this.config = this.appService.getConfig();
    }
}