import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing} from "../../../app.routing";
import {AlertComponent} from "./../alert/alert.component";

@NgModule({
    imports:      [
        BrowserModule,
        routing
    ],
    declarations: [
        AlertComponent
    ],
    exports: [
        AlertComponent
    ]
})
export class AlertModule { }

