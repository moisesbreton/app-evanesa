import {Component, OnInit, Input} from '@angular/core';
import {AppService} from "../../../app.service";

@Component({
    selector: 'fa-alert',
    moduleId: module.id,
    templateUrl: 'alert.component.html',
    providers: [AppService]
})

export class AlertComponent implements OnInit {
    @Input() alert:any;
    hide:boolean = true;
    config: any;
    captions: any;

    constructor(private appService: AppService) {}

    ngOnInit(): any {
        this.config = this.appService.getConfig();
        this.captions = this.appService.getCaptions();
        if (this.alert) {
            this.hide = false;
        }
    }

    onClickHide() {
        this.hide = true;
    }
}