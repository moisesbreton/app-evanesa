import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AgendaComponent} from "./agenda.component";
import {routing} from "../../app.routing";
import {FormsModule} from "@angular/forms";
import {AlertModule} from "../shared/alert/alert.module";
import {LoadingModule} from "../shared/loading/loading.module";
import {CalendarMainModule} from "./calendar/calendar.module";
import {SegmentComponent} from "./segment/segment.component";
import {SegmentFormComponent} from "./segment/segment-form.component";

@NgModule({
    imports: [
        BrowserModule,
        routing,
        FormsModule,
        AlertModule,
        LoadingModule,
        CalendarMainModule,
    ],
    declarations: [
        AgendaComponent,
        SegmentComponent,
        SegmentFormComponent
    ],
    entryComponents: [
        SegmentFormComponent
    ],
    bootstrap:    [ AgendaComponent ]
})
export class AgendaModule { }

