import {Component, OnInit, Inject, forwardRef} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AppService} from '../../../app.service';
import {DialogRef, CloseGuard, ModalComponent} from 'angular2-modal';
import {BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {AlertState} from '../../../config/constants/alert.constant';
import {Segment, PopUpActions, SegmentHelper} from '../../../config/interfaces/segment.interface';
import {Day} from '../../../config/constants/day.enum';
import {TimeRange} from '../../../config/constants/time-range.constant';

export class CustomModalContext extends BSModalContext {
    public segment: Segment;
    public popUpAction: PopUpActions;
}

@Component({
    selector: 'app-segment-form',
    moduleId: module.id,
    templateUrl: 'segment-form.component.html',
    providers: [AppService]
})

export class SegmentFormComponent implements OnInit, ModalComponent<CustomModalContext> {
    captions: any;
    days: Array<Object> = [];
    context: CustomModalContext;
    edit = false;
    segment: Segment;
    popUpAction: PopUpActions;
    loading = false;
    displayAlert = false;
    alert: any = {};
    timeRangeFrom: Array<{value: string, name: string, disabled: boolean}> = [];
    timeRangeTo: Array<{value: string, name: string, disabled: boolean}> = [];

    constructor(private appService: AppService, public dialog: DialogRef<CustomModalContext>) {
        this.context = this.dialog.context;
        // this.dialog.setCloseGuard(this);
    }

    ngOnInit() {
        this.captions = this.appService.getCaptions();
        if (this.context.segment) {
            this.segment = this.context.segment;
            this.edit = true;
        } else {
            this.segment = {
                id: null,
                start_time: '08:00',
                end_time: '18:00',
                time_per_appointment: 30,
                day: Day.Monday,
                overbooking: false,
                overbooking_number: 3
            };
        }

        if (this.context.popUpAction) {
            this.popUpAction = this.context.popUpAction;
        }

        this.setDays();
        this.setTimeRanges();
    }

    save(form: NgForm) {

        // Some extra validation needs to happen here!!! Like confirm password
        this.loading = true;
        const userInfo = this.appService.getLoggedUserInfo();

        // Assuming that we have all the fields
        const params: any = {
            start_time: form.value.start_time,
            end_time: form.value.end_time,
            overbooking: form.value.overbooking,
            time_per_appointment: form.value.time_per_appointment,
            overbooking_number: form.value.overbooking_number,
            day: form.value.day
        };

        let method = 'POST';
        if (this.edit) {
            method = 'PUT';
            params.id = this.segment.id;
        }

        // Submitting the user
        this.appService.apiRequestToken(
            '/calendar/segments',
            method,
            params
        ).subscribe(
            result => {
                this.loading = false;
                if (result.success) {
                    this.popUpAction.onChange();
                    this.dialog.close();
                } else {
                    this.alert.message = result.message;
                    this.alert.status = AlertState.error;
                    this.displayAlert = true;
                }
            }
        );
    }

    setDays() {
        for (const d in Day) {
            const isValueProperty = parseInt(d, 10) >= 0
            if (isValueProperty) {
                this.days.push({
                    id: d,
                    name: Day[d].toLowerCase()
                });
            }
        }
    }

    setTimeRanges() {
        for (const t of TimeRange) {
            this.timeRangeFrom.push({
                value: t.format_24,
                name: t.format_12,
                disabled: false
            });

            this.timeRangeTo.push({
                value: t.format_24,
                name: t.format_12,
                disabled: false
            });
        }
        this.checkTimeRanges();
    }

    checkTimeRanges() {
        const segmentHelper = new SegmentHelper();
        const indexFrom = this.timeRangeFrom.findIndex(function(o) {
            return o.name === segmentHelper.getTimeFormat(this.segment.start_time);
        }.bind(this));

        const indexTo = this.timeRangeTo.findIndex(function(o) {
            return o.name === segmentHelper.getTimeFormat(this.segment.end_time);
        }.bind(this));

        for (let i = indexTo; i < this.timeRangeFrom.length; i++) {
            this.timeRangeFrom[i].disabled = true;
        }

        for (let i = indexFrom; i >= 0; i--) {
            this.timeRangeTo[i].disabled = true;
        }
    }

    timeRangeChanged(event, start: any, end: any) {
        console.log('CHANGED!');
        // this.segment.start = TimeRange[start];
        // this.segment.end = TimeRange[end];
    }

    onClickCancel() {
        this.dialog.close();
    }

    beforeDismiss() {
        if (this.popUpAction) {
            this.popUpAction.onChange();
        }
    }

}