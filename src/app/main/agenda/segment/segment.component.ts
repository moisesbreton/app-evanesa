import {Component, OnInit, Input, ViewContainerRef} from '@angular/core';
import {AppService} from '../../../app.service';
import {Overlay, overlayConfigFactory} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {SegmentFormComponent} from './segment-form.component';
import {Segment, PopUpActions, SegmentHelper} from '../../../config/interfaces/segment.interface';
import {Day} from '../../../config/constants/day.enum';


@Component({
    selector: 'fa-segment',
    moduleId: module.id,
    templateUrl: './segment.component.html',
    providers: [AppService, Modal]
})

export class SegmentComponent implements OnInit {
    captions: any;
    segments: Array<Segment> = [];
    days: Array<Object> = [];
    segmentHelper = new SegmentHelper();

    constructor(private appService: AppService, overlay: Overlay, vcRef: ViewContainerRef, public modal: Modal) {
        // overlay.defaultViewContainer = vcRef;
    }

    ngOnInit(): any {
        this.captions = this.appService.getCaptions();

        // this.segments = [
        //     {
        //         id: 235,
        //         start: '09:00',
        //         end: '13:00',
        //         span: 20,
        //         day: Day.Monday,
        //         overbooking: true,
        //         max_overbooking: 3,
        //     },
        //     {
        //         id: 245,
        //         start: '15:00',
        //         end: '19:00',
        //         span: 20,
        //         day: Day.Monday,
        //         overbooking: false
        //     },
        //     {
        //         start: '09:00',
        //         end: '13:00',
        //         overbooking: true,
        //         max_overbooking: 3,
        //         span: 20,
        //         id: 256,
        //         day: Day.Wednesday
        //     },
        //     {
        //         start: '15:00',
        //         end: '19:00',
        //         overbooking: false,
        //         max_overbooking: 0,
        //         span: 20,
        //         id: 355,
        //         day: Day.Wednesday
        //     },
        //     {
        //         start: '09:00',
        //         end: '13:00',
        //         overbooking: true,
        //         max_overbooking: 4,
        //         span: 20,
        //         id: 564,
        //         day: Day.Friday
        //     }
        // ];

        this.setDays();
        this.getSegments();
    }

    editAddSegment(segment: Segment) {
        const _popupactions: PopUpActions = {
            onChange: this.onChange
        };
        this.modal.open(SegmentFormComponent, overlayConfigFactory({ segment: segment, popUpAction: _popupactions }, BSModalContext));
    }

    setDays() {
        for (const d in Day) {
            const isValueProperty = parseInt(d, 10) >= 0
            if (isValueProperty) {
                this.days.push({
                    id: d,
                    name: Day[d].toLowerCase()
                });
            }
        }
    }

    getSegmentsOfDay(day: number) {
        return this.segmentHelper.getSegmentsOfDay(day, this.segments);
    }

    private getTimeFormat(time: string) {
        return this.segmentHelper.getTimeFormat(time);
    }

    getSegments() {
        // Get calendar ID first
        const userInfo = this.appService.getLoggedUserInfo();
        this.appService.apiRequestToken(
            '/calendar/' + userInfo.company_id, 'GET', {}
        ).subscribe(
            result => {
                if (result.success) {
                    this.getSegmentsData(result.data.calendar_id);
                } else {
                    // TODO
                    // Should I render an error message?
                }
            }
        );
    }

    getSegmentsData(calendar_id: number) {
        this.appService.apiRequestToken(
            '/calendar/segments/' + calendar_id, 'GET', {}
        ).subscribe(
            result => {
                if (result.success) {
                    this.segments = result.data;
                } else {
                    // TODO
                    // Should I render an error message?
                }
            }
        );
    }

    onChange() {
        console.log('Changed!');
    }
}
