import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing} from "../../../app.routing";
import {CalendarComponent} from "./calendar.component";
import {CalendarModule} from "../../../lib/calendar/calendar.module";
import {LoadingModule} from "../../shared/loading/loading.module";

@NgModule({
    imports:      [
        BrowserModule,
        routing,
        CalendarModule.forRoot()
    ],
    declarations: [
        CalendarComponent
    ],
    exports: [
        CalendarComponent
    ],
    entryComponents: [
        CalendarComponent
    ]
})
export class CalendarMainModule { }

