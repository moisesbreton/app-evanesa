import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {config} from '../../../config/config';
import {
    startOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addWeeks,
    subWeeks,
    addMonths,
    subMonths,
    addHours, getDay, addMinutes
} from 'date-fns';
import { Subject } from 'rxjs/Subject';
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarViewMoreAction,
    CalendarAddNewAction
} from '../../../lib/calendar/index';
import {AppService} from '../../../app.service';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {Overlay, overlayConfigFactory} from 'angular2-modal';
import {CalendarFormComponent} from '../../../lib/calendar/components/common/calendarForm.component';
import {PopUpActions} from '../../../lib/calendar/calendar-utils/calendarUtils';
import {Segment, SegmentHelper} from '../../../config/interfaces/segment.interface';
import {Day} from '../../../config/constants/day.enum';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
    selector: 'app-calendar',
    moduleId: module.id,
    styles: [`
        h3 {
          margin: 0;
        }
        .container {
          padding-bottom: 50px;
        }
    `],
    templateUrl: 'calendar.component.html'
})

export class CalendarComponent implements OnInit {
    captions: any;
    language = 'es';
    view = 'month';
    viewDate: Date = new Date();
    segmentsAvailable = false;
    eventsAvailable = false;

    actions: CalendarEventAction[] = [{
        label: '<i class=\'fa fa-fw fa-pencil\'></i>',
        onClick: ({event}: {event: CalendarEvent}): void => {
            this.editEvent(event);
        }
    }, {
        label: '<i class=\'fa fa-fw fa-times\'></i>',
        onClick: ({event}: {event: CalendarEvent}): void => {
            this.events = this.events.filter(iEvent => iEvent !== event);
        }
    }];

    viewMoreAction: CalendarViewMoreAction = {
        onClick: (): void => {
            this.view = 'day';
        }
    };

    addNewAction: CalendarAddNewAction = {
        onClick: (date: Date): void => {
            this.addEvent(date);
        }
    };
    refresh: Subject<any> = new Subject();

    segments: Array<Segment>;

    events: CalendarEvent[] = [{
        start: addHours(addDays(startOfDay(new Date()), 0), 9),
        end: addMinutes(addHours(addDays(startOfDay(new Date()), 0), 9), 20),
        title: 'Normal Event',
        color: colors.red,
        actions: this.actions
    },{
        start: addMinutes(addHours(addDays(startOfDay(new Date()), 0), 9), 20),
        end: addMinutes(addHours(addDays(startOfDay(new Date()), 0), 9), 40),
        title: 'Normal Event 2',
        color: colors.red,
        actions: this.actions
    }];

    activeDayIsOpen = false;
    private sub: any;
    urlPath: string;

    constructor(private appService: AppService, overlay: Overlay, vcRef: ViewContainerRef, public modal: Modal,
                private route: ActivatedRoute, private router: Router) {
        // overlay.defaultViewContainer = vcRef;
    }

    ngOnInit() {
        this.urlPath = window.location.pathname;
        this.captions = this.appService.getCaptions();
        if (config.defaultLanguage) {
            this.language = config.defaultLanguage.toLowerCase();
        }

        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                this.checkParams();
            }
        });

        this.getSegments();
    }

    checkParams() {
        this.sub = this.route.queryParams.subscribe(params => {
            let view = 'month';
            let date = this.viewDate.toString();

            if (params['v']) {
                view = params['v'];
            }
            if (params['d']) {
                date = params['d'];
            }

            this.view = view;
            this.viewDate = new Date(date);
        });

        // this.getData();
    }

    onChange() {
        console.log('CAMBIE!');
    }

    increment(): void {

        const addFn: any = {
            day: addDays,
            week: addWeeks,
            month: addMonths
        }[this.view];

        let d = addFn(this.viewDate, 1);
        this.appService.redirectToURL(this.urlPath + '?v=' + this.view + '&d=' + d.toString());
        // this.viewDate = addFn(this.viewDate, 1);

    }

    decrement(): void {

        const subFn: any = {
            day: subDays,
            week: subWeeks,
            month: subMonths
        }[this.view];

        let d = subFn(this.viewDate, 1);
        this.appService.redirectToURL(this.urlPath + '?v=' + this.view + '&d=' + d.toString());
    }

    today(): void {
        let d = new Date();
        this.appService.redirectToURL(this.urlPath + '?v=' + this.view + '&d=' + d.toString());
        // this.viewDate = new Date();
    }

    changeView(view: string) {
        let d = this.viewDate;
        this.appService.redirectToURL(this.urlPath + '?v=' + view + '&d=' + d.toString());
    }

    dayClicked({date, events}: {date: Date, events: CalendarEvent[]}): void {
        const segmentHelper = new SegmentHelper();
        if (isSameMonth(date, this.viewDate)) {
            // if (
            //     (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
            //     events.length === 0
            // ) {
            //     this.activeDayIsOpen = false;
            // } else {
            //     this.activeDayIsOpen = true;
            //     this.viewDate = date;
            // }
            if (segmentHelper.hasSegments(getDay(date), this.segments)) {
                this.appService.redirectToURL(this.urlPath + '?v=day' + '&d=' + date.toString());
            }
        }
    }

    eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        this.refresh.next();
    }

    addEvent(date: Date) {
        let hasEvent = false;
        for (const event of this.events) {
            if (event.start.toString() === date.toString()) {
                hasEvent = true;
                break;
            }
        }

        if (!hasEvent) {
            const _popupactions: PopUpActions = {
                onClose: this.onChange
            };
            this.modal.open(
                CalendarFormComponent,
                overlayConfigFactory (
                    { event: null, popUpActions: _popupactions, viewDate: date },
                    BSModalContext
                )
            );
        }
    }

    editEvent(event: CalendarEvent) {
        this.modal.open(CalendarFormComponent, overlayConfigFactory({ event: event }, BSModalContext));
    }

    getSegments() {
        // Get calendar ID first
        const userInfo = this.appService.getLoggedUserInfo();
        this.appService.apiRequestToken(
            '/calendar/' + userInfo.company_id, 'GET', {}
        ).subscribe(
            result => {
                if (result.success) {
                    this.getSegmentsData(result.data.calendar_id);
                } else {
                    // TODO
                    // Should I render an error message?
                }
            }
        );
    }

    getSegmentsData(calendar_id: number) {
        this.appService.apiRequestToken(
            '/calendar/segments/' + calendar_id, 'GET', {}
        ).subscribe(
            result => {
                if (result.success) {
                    this.segments = result.data;
                    this.segmentsAvailable = true;
                    this.eventsAvailable = true; // This have to be place when we look the events on the API.
                } else {
                    // TODO
                    // Should I render an error message?
                }
            }
        );
    }

}