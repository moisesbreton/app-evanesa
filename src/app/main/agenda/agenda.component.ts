import {Component, OnInit} from '@angular/core';
import {AppService} from "../../app.service";

@Component({
    selector: 'fa-agenda',
    templateUrl: './agenda.component.html',
    moduleId: module.id
})

export class AgendaComponent implements OnInit {
    captions:any;

    constructor(private appService: AppService) {}

    ngOnInit() {
        this.captions = this.appService.getCaptions();
    }

}