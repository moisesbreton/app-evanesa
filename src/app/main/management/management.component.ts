import {Component, OnInit} from '@angular/core';
import {AppService} from "../../app.service";

@Component({
    selector: 'fa-management',
    moduleId: module.id,
    templateUrl: './management.component.html',
    providers: [AppService]
})

export class ManagementComponent implements OnInit {
    captions:any;

    constructor(private appService: AppService) {}

    ngOnInit() {
        this.captions = this.appService.getCaptions();
    }

}