import {Component, OnInit, forwardRef, Inject} from '@angular/core';
import {AppService} from '../../../app.service';
import {DialogRef, CloseGuard, ModalComponent} from 'angular2-modal';
import {BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {AlertState} from '../../../config/constants/alert.constant';
import {User} from '../../../config/interfaces/user.interface';
import {ManagementUsersComponent} from './management-users.component';

export class CustomModalContext extends BSModalContext {
    public user: any;
    public parent: ManagementUsersComponent;
}

@Component({
    selector: 'app-delete-user',
    moduleId: module.id,
    templateUrl: './delete-user.component.html',
    providers: [AppService]
})

export class DeleteUserComponent implements OnInit, ModalComponent<CustomModalContext> {
    captions: any;
    context: CustomModalContext;
    user: User;
    loading = false;
    displayAlert = false;
    alert: any = {};
    _parent: ManagementUsersComponent;

    constructor(private appService: AppService, public dialog: DialogRef<CustomModalContext>) {
        this.context = dialog.context;
        this._parent = this.context.parent;
        // dialog.setCloseGuard(this);
    }

    ngOnInit() {
        this.captions = this.appService.getCaptions();
        if (!this.context.user) {
            this.dialog.close();
        }

        this.user = this.context.user;
    }

    delete() {
        this.loading = true;

        // Submitting the user
        const userInfo = this.appService.getLoggedUserInfo();
        this.appService.apiRequestToken(
            '/user/' + this.user.id,
            'DELETE',
            null
        ).subscribe(
            result => {
                this.loading = false;
                if (result.success) {
                    this._parent.onChange();
                    this.dialog.close();
                } else {
                    this.alert.message = result.message;
                    this.alert.status = AlertState.error;
                    this.displayAlert = true;
                }
            }
        );
    }

    beforeClose() {
        return true;
    }

    onClickCancel() {
        this.dialog.close();
    }
}
