import {Component, OnInit, ViewContainerRef, Input} from '@angular/core';
import {AppService} from '../../../app.service';
import {Overlay, overlayConfigFactory} from 'angular2-modal';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {UserFormComponent} from './user-form.component';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';


@Component({
    selector: 'app-management-users',
    moduleId: module.id,
    templateUrl: './management-users.component.html',
    providers: [AppService, Modal]
})

export class ManagementUsersComponent implements OnInit {

    captions: any;
    usersResults: Array<Object>;
    maxResult: number;
    private sub: any;
    page = 1;

    constructor(private appService: AppService, overlay: Overlay, vcRef: ViewContainerRef, public modal: Modal,
                private route: ActivatedRoute, private router: Router) {
        // overlay.defaultViewContainer = vcRef;
    }

    ngOnInit(): any {
        this.captions = this.appService.getCaptions();

        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                this.checkPageParams();
            }
        });

        this.getData();
    }

    checkPageParams() {
        this.sub = this.route.queryParams.subscribe(params => {
            this.page = 1;
            if (params['page']) {
                this.page = +params['page'];
            }
        });

        this.getData();
    }

    getData() {
        // Get users
        const userInfo = this.appService.getLoggedUserInfo();
        this.appService.apiRequestToken(
            '/company/users',
            'GET',
            {
                companyid: userInfo.company_id,
                page: this.page,
                pagesize: 10,
                sort: 'first_name',
                order: 'DESC'
            }
        ).subscribe(
            result => {
                if (result.data.length > 0) {
                    this.usersResults = result.data;
                    this.maxResult = result.totalRecords;
                } else {
                    // TODO
                    // Should I render an error message?
                }
            }
        );
    }

    onClickAddUser(u: any) {
        this.modal.open(UserFormComponent, overlayConfigFactory({ user: false, parent: this }, BSModalContext));
    }

    onChange() {
        this.getData();
    }
}
