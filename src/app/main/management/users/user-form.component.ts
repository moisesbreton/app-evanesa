import {Component, OnInit, forwardRef, Inject} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AppService} from '../../../app.service';
import {DialogRef, CloseGuard, ModalComponent} from 'angular2-modal';
import {BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {AlertState} from '../../../config/constants/alert.constant';
import {User} from '../../../config/interfaces/user.interface';
import {Role} from '../../../config/constants/role.enum';
import {ManagementUsersComponent} from './management-users.component';

export class CustomModalContext extends BSModalContext {
    public user: any;
    public parent: ManagementUsersComponent;
}

@Component({
    selector: 'app-user-form',
    moduleId: module.id,
    templateUrl: './user-form.component.html',
    providers: [AppService]
})

export class UserFormComponent implements OnInit, ModalComponent<CustomModalContext> {
    captions: any;
    roles: Array<Object>;
    context: CustomModalContext;
    edit = false;
    user: User;
    loading = false;
    displayAlert = false;
    alert: any = {};
    _parent: ManagementUsersComponent;

    constructor(private appService: AppService, public dialog: DialogRef<CustomModalContext>) {
        this.context = this.dialog.context;
        this._parent = this.context.parent;
        // this.dialog.setCloseGuard(this);
    }

    ngOnInit() {
        this.captions = this.appService.getCaptions();
        if (this.context.user) {
            this.user = this.context.user;
            this.edit = true;
        } else {
            this.user = {
                id: null,
                first_name: '',
                last_name: '',
                user_name: '',
                role_id: Role.None
            };
        }

        // Get users
        this.appService.apiRequest(
            '/dropdown/roles',
            'GET',
            null
        ).subscribe(
            result => {
                if (result.data.length > 0) {
                    this.roles = result.data;
                } else {
                    // TODO
                    // Should I render an error message?
                }
            }
        );
    }

    save(form: NgForm) {
        // Some extra validation needs to happen here!!! Like confirm password
        if (form.value.password !== form.value.confirm_password) {
            this.alert.message = this.captions.get('password_dont_match');
            this.alert.status = AlertState.error;
            this.displayAlert = true;
            return;
        }

        this.loading = true;
        const userInfo = this.appService.getLoggedUserInfo();

        // Assuming that we have all the fields
        const params: any = {
            first_name: form.value.first_name,
            last_name: form.value.last_name,
            user_name: form.value.user_name,
            password: form.value.password,
            role_id: form.value.roles,
            company_id: userInfo.company_id
        };

        let method = 'POST';
        if (this.edit) {
            method = 'PUT';
            params.user_id = this.user.id;
            params.membership_id = this.user.membership_id;
        }

        // Submitting the user
        this.appService.apiRequestToken(
            '/user',
            method,
            params
        ).subscribe(
            result => {
                this.loading = false;
                if (result.success) {
                    this._parent.onChange();
                    this.dialog.close();
                } else {
                    this.alert.message = result.message;
                    this.alert.status = AlertState.error;
                    this.displayAlert = true;
                }
            }
        );
    }

    onClickCancel() {
        this.dialog.close();
    }

    beforeDismiss() {
        this._parent.onChange();
    }
}
