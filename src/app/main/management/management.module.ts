import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ManagementComponent} from "./management.component";
import {ManagementUsersComponent} from "./users/management-users.component";
import {ManagementRolesComponent} from "./roles/management-roles.component";
import {routing} from "../../app.routing";
import {UserFormComponent} from "./users/user-form.component";
import {FormsModule} from "@angular/forms";
import {DeleteUserComponent} from "./users/delete-user.component";
import {AlertModule} from "../shared/alert/alert.module";
import {LoadingModule} from "../shared/loading/loading.module";
import {PaginationModule} from "../shared/pagination/pagination.module";
import {TableUserModule} from "../shared/table-user/table-user.module";

@NgModule({
    imports:      [
        BrowserModule,
        routing,
        FormsModule,
        AlertModule,
        LoadingModule,
        PaginationModule,
        TableUserModule
    ],
    declarations: [
        ManagementComponent,
        ManagementUsersComponent,
        ManagementRolesComponent,
        UserFormComponent,
        DeleteUserComponent
    ],
    entryComponents: [
        UserFormComponent,
        DeleteUserComponent
    ],
    bootstrap:    [ ManagementComponent ]
})
export class ManagementModule { }

