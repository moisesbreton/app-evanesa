import {Component, OnInit} from '@angular/core';
import {AppService} from "../../../app.service";

@Component({
    selector: 'fa-management-roles',
    moduleId: module.id,
    templateUrl: './management-roles.component.html',
    providers: [AppService]
})

export class ManagementRolesComponent implements OnInit {
    captions: any;

    constructor(private appService: AppService) {}

    ngOnInit():any {
        this.captions = this.appService.getCaptions();
    }
}
