"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var app_service_1 = require("../app.service");
var login_service_1 = require("./login.service");
var LoginComponent = (function () {
    function LoginComponent(loginService, appService) {
        this.loginService = loginService;
        this.appService = appService;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.captions = this.appService.getCaptions();
        this.logo = this.appService.getLogoURL();
    };
    LoginComponent.prototype.onSubmit = function (form) {
        var username = form.value.username;
        var password = form.value.password;
        var response = this.loginService.doLogin(username, password);
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'fa-login',
            moduleId: module.id,
            templateUrl: './login.component.html',
            providers: [app_service_1.AppService, login_service_1.LoginService]
        }), 
        __metadata('design:paramtypes', [login_service_1.LoginService, app_service_1.AppService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map