import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {AppService} from "../app.service";
import {LoginService} from "./login.service";

@Component({
    selector: 'fa-login',
    moduleId: module.id,
    templateUrl: './login.component.html',
    providers: [AppService, LoginService]
})

export class LoginComponent implements OnInit {
    captions: any;
    logo: any;
    config: any;

    constructor(private loginService: LoginService, private appService: AppService) {

    }

    ngOnInit():any {
        var loginUser = this.appService.checkLoginUser();

        if (loginUser)
        {
            this.appService.redirectToURL('/main');
            return;
        }

        this.config = this.appService.getConfig();
        this.captions = this.appService.getCaptions();
        this.logo = this.config.staticURL + "/images/logo_login_2.png";
    }

    onSubmit(form: NgForm){
        var username = form.value.username;
        var password = form.value.password;

        var response = this.loginService.doLogin(username, password);
    }

}
