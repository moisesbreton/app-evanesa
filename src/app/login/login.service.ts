import {Injectable} from "@angular/core";
import {AppService} from "../app.service";

@Injectable()
export class LoginService {
    constructor(private appService: AppService) { }

    doLogin(username, password) {
        var params = {
            username: username,
            password: password
        };

        this.appService.apiRequest("/authenticate", "post", params)
            .subscribe(
                result => this.processLogin(result)
            );
    }

    processLogin(result) {
        if(!result.success) {
            alert(result.message);
        } else {
            this.appService.storeTokenUser(result.data);
            this.appService.redirectToURL('/main');
        }
    }
}