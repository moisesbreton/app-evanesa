import { CalendarEvent } from '../calendar-utils/calendarUtils';

export interface CalendarEventTimesChangedEvent {
    event: CalendarEvent;
    newStart: Date;
    newEnd: Date;
}
