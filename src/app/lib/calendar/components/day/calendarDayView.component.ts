import {
  Component,
  ChangeDetectionStrategy,
  Input,
  OnChanges,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  LOCALE_ID,
  Inject,
  OnInit,
  OnDestroy
} from '@angular/core';
import {
  getDayView, getDayViewHourGrid, CalendarEvent, DayView, DayViewHour,
  AddNewAction
} from '../../calendar-utils/calendarUtils';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import {Segment, SegmentHelper} from "../../../../config/interfaces/segment.interface";
import {getDay, getHours, getSeconds} from "date-fns";
import {AppService} from "../../../../app.service";
import * as moment from "moment/moment";

const SEGMENT_HEIGHT: number = 30;

@Component({
  selector: 'mwl-calendar-day-view',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="cal-day-view">
      <div class="day-segments" *ngFor="let segment of daySegments">
          <h5>
              {{captions.get('time_range_from') | uppercase}} : {{getTimeFormat(segment.start_time)}} | {{captions.get('time_range_to') | uppercase}} : {{getTimeFormat(segment.end_time)}}
          </h5>
        <div class="cal-hour-rows">
          <table class="table  table-hover general-table" style="margin-bottom: 0 !important;">
              <tbody>
                <tr *ngFor="let time of getTimesOfSegment(segment)" (click)="addNewAction.onClick(getDateWithTime(time))" style="cursor:pointer;">
                    <td style="width: 80px; vertical-align: middle !important;">
                        {{ time }}
                    </td>
                    <td *ngIf="getEventWithStartTime(time) as event; else noEvent"> 
                        <div class="alert-info fade in cal-event">
                          {{event.title}}
                          <mwl-calendar-event-actions [event]="event"></mwl-calendar-event-actions>
                        </div>
                    </td>
                    <ng-template #noEvent>
                        <td>
                        </td>
                    </ng-template>
                </tr>
              </tbody>
          </table>
        </div>
      </div>
        <div  *ngIf="daySegments.length == 0" class="alert alert-warning ">
            <span class="alert-icon"><i class="fa fa-bell-o"></i></span>
            <div class="notification-info">
                <ul class="clearfix notification-meta">
                    <li class="pull-left notification-sender">{{captions.get('no_time_for_this_day') | uppercase}}</li>
                </ul>
                <p>
                    {{captions.get('please_add_a_segment_for_this_day')}} | <a (click)="goSegment()">{{captions.get('add_segment')}}</a>
                </p>
            </div>
        </div>
    </div>
  `
})
export class CalendarDayViewComponent implements OnChanges, OnInit, OnDestroy {

  /**
   * The current view date
   */
  @Input() viewDate: Date;

  /**
   * An array of events to display on view
   */
  @Input() events: CalendarEvent[] = [];

  /**
   * An array of events to display on view
   */
  @Input() segments: Segment[] = [];

  /**
   * The number of segments in an hour. Must be <= 6
   */
  @Input() hourSegments: number = 2;

  /**
   * The day start hours in 24 hour time. Must be 0-23
   */
  @Input() dayStartHour: number = 0;

  /**
   * The day start minutes. Must be 0-59
   */
  @Input() dayStartMinute: number = 0;

  /**
   * The day end hours in 24 hour time. Must be 0-23
   */
  @Input() dayEndHour: number = 23;

  /**
   * The day end minutes. Must be 0-59
   */
  @Input() dayEndMinute: number = 59;

  /**
   * The width in pixels of each event on the view
   */
  @Input() eventWidth: number = 150;

  /**
   * An observable that when emitted on will re-render the current view
   */
  @Input() refresh: Subject<any>;

  /**
   * The locale used to format dates
   */
  @Input() locale: string;

  /**
   * A function that will be called before each hour segment is called. The first argument will contain the hour segment.
   * If you add the `cssClass` property to the segment it will add that class to the hour segment in the template
   */
  @Input() hourSegmentModifier: Function;

  /**
   * Called when an event title is clicked
   */
  @Output() eventClicked: EventEmitter<{event: CalendarEvent}> = new EventEmitter<{event: CalendarEvent}>();

  /**
   * Called when an hour segment is clicked
   */
  @Output() hourSegmentClicked: EventEmitter<{date: Date}> = new EventEmitter<{date: Date}>();

  @Input() addNewAction: AddNewAction;

  hours: DayViewHour[] = [];
  view: DayView;
  width: number = 0;
  refreshSubscription: Subscription;
  daySegments: Segment[] = [];
  captions: any;

  constructor(private cdr: ChangeDetectorRef, @Inject(LOCALE_ID) locale: string, private appService: AppService) {
    this.locale = locale;
  }

  ngOnInit(): void {
    this.captions = this.appService.getCaptions();
    if (this.refresh) {
      this.refreshSubscription = this.refresh.subscribe(() => {
        this.refreshAll();
        this.cdr.markForCheck();
      });
    }
    let segmentHelper = new SegmentHelper();
    this.daySegments = segmentHelper.getSegmentsOfDay(getDay(this.viewDate), this.segments);
  }

  ngOnDestroy(): void {
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
  }

  ngOnChanges(changes: any): void {

    if (
      changes.viewDate ||
      changes.dayStartHour ||
      changes.dayStartMinute ||
      changes.dayEndHour ||
      changes.dayEndMinute
    ) {
      this.refreshHourGrid();
    }

    if (
      changes.viewDate ||
      changes.events ||
      changes.dayStartHour ||
      changes.dayStartMinute ||
      changes.dayEndHour ||
      changes.dayEndMinute ||
      changes.eventWidth
    ) {
      this.refreshView();
    }

  }

  private refreshHourGrid(): void {
    this.hours = getDayViewHourGrid({
      viewDate: this.viewDate,
      hourSegments: this.hourSegments,
      dayStart: {
        hour: this.dayStartHour,
        minute: this.dayStartMinute
      },
      dayEnd: {
        hour: this.dayEndHour,
        minute: this.dayEndMinute
      }
    });
    if (this.hourSegmentModifier) {
      this.hours.forEach(hour => {
        hour.segments.forEach(segment => this.hourSegmentModifier(segment));
      });
    }

    let segmentHelper = new SegmentHelper();
    this.daySegments = segmentHelper.getSegmentsOfDay(getDay(this.viewDate), this.segments);
  }

  private refreshView(): void {
    this.view = getDayView({
      events: this.events,
      viewDate: this.viewDate,
      hourSegments: this.hourSegments,
      dayStart: {
        hour: this.dayStartHour,
        minute: this.dayStartMinute
      },
      dayEnd: {
        hour: this.dayEndHour,
        minute: this.dayEndMinute
      },
      eventWidth: this.eventWidth,
      segmentHeight: SEGMENT_HEIGHT
    });

    let segmentHelper = new SegmentHelper();
    this.daySegments = segmentHelper.getSegmentsOfDay(getDay(this.viewDate), this.segments);
  }

  private refreshAll(): void {
    this.refreshHourGrid();
    this.refreshView();
  }

  private getTimeFormat(time:string) {
    let segmentHelper = new SegmentHelper();
    return segmentHelper.getTimeFormat(time);
  }

  getTimesOfSegment(segment:Segment) {
    var start = moment(segment.start_time, 'h:mm:ss');
    var end = moment(segment.end_time, 'h:mm:ss');
    var listTimes = [];

    while(start <= end)
    {
      listTimes.push(start.format('h:mm A'));
      start.add(segment.time_per_appointment, 'minutes');
    }

    return listTimes;
  }

  getEventWithStartTime(time:string) {
    let today = this.getDateWithTime(time);
    for (let event of this.events) {
      if(event.start.toString() == today.toString()) {
        return event;
      }
    }

    return false;
  }

  getDateWithTime(time:string) {
    return moment(this.viewDate.getUTCFullYear()+"/"+(this.viewDate.getUTCMonth()+1)+"/"+this.viewDate.getUTCDate()+" "+time, 'YYYY/MM/DD h:mm A').toDate()
  }

  goSegment() {
    this.appService.redirectToURL('/main/agenda/segment');
  }
}
