import {
  Component,
  Input,
  ChangeDetectionStrategy,
  trigger,
  style,
  transition,
  animate,
  Output,
  EventEmitter,
  OnInit
} from '@angular/core';
import {CalendarEvent, ViewMoreAction, AddNewAction} from '../../calendar-utils/calendarUtils';
import {AppService} from "../../../../app.service";

@Component({
  selector: 'mwl-calendar-open-day-events',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="cal-open-day-events" [@collapse] *ngIf="isOpen">
      <div
        *ngFor="let event of events"
        [ngClass]="event?.cssClass">
        <span class="cal-event" [style.backgroundColor]="event.color.primary"></span>
        <mwl-calendar-event-title
          [event]="event"
          view="month"
          (click)="eventClicked.emit({event: event})">
        </mwl-calendar-event-title>
        <mwl-calendar-event-actions [event]="event"></mwl-calendar-event-actions>
      </div>
      <div class="btn-group pull-right top10">
            <button 
                class="btn btn-primary" 
                id="view_more" 
                (click)="viewMoreAction.onClick()">
                {{captions.get('view_more_details')}}
            </button>
        </div>
        <div class="btn-group pull-left top10">
            <button 
                class="btn btn-primary" 
                id="add_new" 
                (click)="addNewAction.onClick()">
                {{captions.get('add_new_event')}}
            </button>
        </div>
    </div>
  `,
  animations: [
    trigger('collapse', [
      transition('void => *', [
        style({height: 0}),
        animate('150ms linear', style({height: '*'}))
      ]),
      transition('* => void', [
        style({height: '*'}),
        animate('150ms linear', style({height: 0}))
      ])
    ])
  ]
})
export class CalendarOpenDayEventsComponent implements OnInit {
  @Input() isOpen: boolean = false;
  @Input() events: CalendarEvent[];
  @Input() viewMoreAction: ViewMoreAction;
  @Input() addNewAction: AddNewAction;
  @Output() eventClicked: EventEmitter<{event: CalendarEvent}> = new EventEmitter<{event: CalendarEvent}>();
  captions: any;

  constructor(private appService: AppService) {}

  ngOnInit() {
    this.captions = this.appService.getCaptions();
  }

}