import {Component, OnInit, forwardRef, Inject} from '@angular/core';
import {NgForm} from "@angular/forms";
import {AppService} from "../../../../app.service";
import {DialogRef, CloseGuard, ModalComponent} from 'angular2-modal';
import {BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {AlertState} from '../../../../config/constants/alert.constant';
import {CalendarEvent, PopUpActions, EventColor} from "../../calendar-utils/calendarUtils";
import {CalendarComponent} from "../../../../main/agenda/calendar/calendar.component";

export class CustomModalContext extends BSModalContext {
    public event:CalendarEvent;
    public popUpActions: PopUpActions;
    public viewDate: Date;
}

@Component({
    selector: 'fa-calendar-form',
    moduleId: module.id,
    templateUrl: './calendarForm.component.html',
    providers: [AppService]
})

export class CalendarFormComponent implements OnInit, ModalComponent<CustomModalContext> {
    captions:any;
    context: CustomModalContext;
    edit:boolean = false;
    event:CalendarEvent;
    viewDate: Date;
    popUpActions: PopUpActions;
    loading:boolean = false;
    displayAlert:boolean = false;
    alert:any = {};

    constructor(private appService: AppService, public dialog: DialogRef<CustomModalContext>) {
        this.context = this.dialog.context;
        // this.dialog.setCloseGuard(this);
    }

    ngOnInit() {
        this.captions = this.appService.getCaptions();
        if (this.context.viewDate) {
            this.viewDate = this.context.viewDate;
        } else {
            this.viewDate = new Date();
        }
        if (this.context.event)
        {
            this.event = this.context.event;
            this.edit = true;
        } else {
            this.event = <CalendarEvent> {
                id: null,
                title: "",
                start: this.viewDate
            };
        }

        if (this.context.popUpActions) {
            this.popUpActions = this.context.popUpActions;
        }

        if (this.context.viewDate) {
            console.log(this.context.viewDate);
        }
    }

    save(form:NgForm) {
        //Some extra validation needs to happen here!!! Like confirm password
        /*if (form.value.password !== form.value.confirm_password) {
            this.alert.message = this.captions.get("password_dont_match");
            this.alert.status = AlertState.error;
            this.displayAlert = true;
            return;
        }

        this.loading = true;
        let userInfo = this.appService.getLoggedUserInfo();

        //Assuming that we have all the fields
        var params:any = {
            first_name: form.value.first_name,
            last_name: form.value.last_name,
            user_name: form.value.user_name,
            password: form.value.password,
            role_id: form.value.roles,
            company_id: userInfo.company_id
        };

        var method = "POST";
        if (this.edit) {
            method = "PUT";
            params.user_id = this.user.id;
            params.membership_id = this.user.membership_id;
        }

        //Submitting the user
        let userInfo = this.appService.getLoggedUserInfo();
        this.appService.apiRequestToken(
            "/user",
            method,
            params
        ).subscribe(
            result => {
                this.loading = false;
                if (result.success) {
                    this._parent.onChange();
                    this.dialog.close();
                } else {
                    this.alert.message = result.message;
                    this.alert.status = AlertState.error;
                    this.displayAlert = true;
                }
            }
        );*/
    }

    onClickCancel() {
        this.dialog.close();
    }

    beforeDismiss() {
        if(this.popUpActions) {
            this.popUpActions.onClose();
        }
    }
}