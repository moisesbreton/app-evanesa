import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions, Request, Response, URLSearchParams} from "@angular/http";
import {Router} from "@angular/router";
import * as CryptoJS from "crypto-js";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {config} from './config/config';
import {CaptionService} from "./shared/caption.service";

@Injectable()
export class AppService {
  private requestResponse: any;
  constructor(
      private http: Http,
      private router: Router,
      private captions: CaptionService
  ) { }

  checkLoginUser() {
    return sessionStorage.getItem("token") && sessionStorage.getItem("user");
  }

  storeTokenUser(data) {
    sessionStorage.setItem("token", data.token);
    sessionStorage.setItem("user", JSON.stringify(data.session));
  }

  doLogout() {
    sessionStorage.clear();
    this.redirectToURL("/login");
  }

  getCaptions() {
    return this.captions.getCaptions();
  }

  getLogoURL() {
    return config.staticURL + config.logoPath;
  }

  redirectToURL(path) {
    window.scrollTo(0,0);
    this.router.navigateByUrl(path);
  }

  getConfig(){
    return config;
  }

  apiRequest(url, method, params, token=null) {
    var date = new Date();
    var time = date.getTime();
    var privateKey = config.privateKey;
    var clientID = config.clientID;
    var requestURL = config.apiURL + url;
    var message = this.buildMessage(time, clientID, params, method);
    var hash = CryptoJS.HmacSHA256(message, privateKey);

    var searchParams  = this.getSearchParams(params);

    var requestHeaders = new Headers();
    requestHeaders.append('api-id', clientID);
    requestHeaders.append('api-time', time.toString());
    requestHeaders.append('api-hash', hash.toString());
    if (token) {
      requestHeaders.append('Authorization', "Bearer " + token);
    }
    if (method.toUpperCase() == "PUT") {
      requestHeaders.append('Content-Type', "application/json");
    }

    var _requestOptions = {
      method: method.toUpperCase(),
      headers: requestHeaders,
      url: requestURL,
      search: null,
      body: null
    };

    if (method.toUpperCase() == "GET") {
      _requestOptions.search = searchParams;
    } else if (method.toUpperCase() == "PUT") {
      _requestOptions.body = JSON.stringify(params);
    } else {
      _requestOptions.body = searchParams;
    }

    var requestOptions:RequestOptions = new RequestOptions(_requestOptions);

    var req = new Request(requestOptions);

    return this.http.request(req).map((response: Response) => response.json());
  }

  apiRequestToken(url, method, params) {
    let token = sessionStorage.getItem("token");
    return this.apiRequest(url, method, params, token);
  }

  getLoggedUserInfo() {
    return JSON.parse(sessionStorage.getItem("user"));
  }

  private buildMessage(time, id, params, method) {
    if (method.toUpperCase() !== "PUT") {
      var urlParams = this.getSearchParams(params);
      var encodedUrlParams = encodeURIComponent(urlParams.toString());
      encodedUrlParams = encodedUrlParams.replace(/%3D/g, "="); //We don't want to encode the '='
      encodedUrlParams = encodedUrlParams.replace(/%26/g, "&"); //Neither the '&'
      return time + id + encodedUrlParams;
    } else {
      var encodedUrlParams = encodeURIComponent(JSON.stringify(params)) + "=";
      encodedUrlParams = encodedUrlParams.replace(".", "_");
      return time + id + encodedUrlParams;
    }
  }

  private getSearchParams(params) {
    var searchParams = new URLSearchParams();
    for (let key in params) {
      searchParams.append(key, params[key]);
    }

    return searchParams;
  }
}