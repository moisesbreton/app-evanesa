export const AlertState = {
    success: { label: 'success', caption: 'alert_success' },
    warning: { label: 'warning', caption: 'alert_warning' },
    error: { label: 'danger', caption: 'alert_error' },
    info: { label: 'info', caption: 'alert_info' }
}