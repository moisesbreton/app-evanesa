export enum Role {
    Administrator = 1,
    Practicioner = 2,
    Assistant = 3,
    Secretary = 4,
    None = 0
}