export const TimeRange = [
    {
        format_24: "00:00:00",
        format_12: "12:00 AM"
    },
    {
        format_24: "00:30:00",
        format_12: "12:30 AM"
    },
    {
        format_24: "01:00:00",
        format_12: "01:00 AM"
    },
    {
        format_24: "01:30:00",
        format_12: "01:30 AM"
    },
    {
        format_24: "02:00:00",
        format_12: "02:00 AM"
    },
    {
        format_24: "02:30:00",
        format_12: "02:30 AM"
    },
    {
        format_24: "03:00:00",
        format_12: "03:00 AM"
    },
    {
        format_24: "03:30:00",
        format_12: "03:30 AM"
    },
    {
        format_24: "04:00:00",
        format_12: "04:00 AM"
    },
    {
        format_24: "04:30:00",
        format_12: "04:30 AM"
    },
    {
        format_24: "05:00:00",
        format_12: "05:00 AM"
    },
    {
        format_24: "05:30:00",
        format_12: "05:30 AM"
    },
    {
        format_24: "06:00:00",
        format_12: "06:00 AM"
    },
    {
        format_24: "06:30:00",
        format_12: "06:30 AM"
    },
    {
        format_24: "07:00:00",
        format_12: "07:00 AM"
    },
    {
        format_24: "07:30:00",
        format_12: "07:30 AM"
    },
    {
        format_24: "08:00:00",
        format_12: "08:00 AM"
    },
    {
        format_24: "08:30:00",
        format_12: "08:30 AM"
    },
    {
        format_24: "09:00:00",
        format_12: "09:00 AM"
    },
    {
        format_24: "09:30:00",
        format_12: "09:30 AM"
    },
    {
        format_24: "10:00:00",
        format_12: "10:00 AM"
    },
    {
        format_24: "10:30:00",
        format_12: "10:30 AM"
    },
    {
        format_24: "11:00:00",
        format_12: "11:00 AM"
    },
    {
        format_24: "11:30:00",
        format_12: "11:30 AM"
    },
    {
        format_24: "12:00:00",
        format_12: "12:00 PM"
    },
    {
        format_24: "12:30:00",
        format_12: "12:30 PM"
    },
    {
        format_24: "13:00:00",
        format_12: "01:00 PM"
    },
    {
        format_24: "13:30:00",
        format_12: "01:30 PM"
    },
    {
        format_24: "14:00:00",
        format_12: "02:00 PM"
    },
    {
        format_24: "14:30:00",
        format_12: "02:30 PM"
    },
    {
        format_24: "15:00:00",
        format_12: "03:00 PM"
    },
    {
        format_24: "15:30:00",
        format_12: "03:30 PM"
    },
    {
        format_24: "16:00:00",
        format_12: "04:00 PM"
    },
    {
        format_24: "16:30:00",
        format_12: "04:30 PM"
    },
    {
        format_24: "17:00:00",
        format_12: "05:00 PM"
    },
    {
        format_24: "17:30:00",
        format_12: "05:30 PM"
    },
    {
        format_24: "18:00:00",
        format_12: "06:00 PM"
    },
    {
        format_24: "18:30:00",
        format_12: "06:30 PM"
    },
    {
        format_24: "19:00:00",
        format_12: "07:00 PM"
    },
    {
        format_24: "19:30:00",
        format_12: "07:30 PM"
    },
    {
        format_24: "20:00:00",
        format_12: "08:00 PM"
    },
    {
        format_24: "20:30:00",
        format_12: "08:30 PM"
    },
    {
        format_24: "21:00:00",
        format_12: "09:00 PM"
    },
    {
        format_24: "21:30:00",
        format_12: "09:30 PM"
    },
    {
        format_24: "22:00:00",
        format_12: "10:00 PM"
    },
    {
        format_24: "22:30:00",
        format_12: "10:30 PM"
    },
    {
        format_24: "23:00:00",
        format_12: "11:00 PM"
    },
    {
        format_24: "23:00:00",
        format_12: "11:30 PM"
    },
    {
        format_24: "23:59:00",
        format_12: "11:59 PM"
    }
]