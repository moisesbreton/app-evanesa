import {Day} from "../constants/day.enum";
import * as moment from "moment/moment";

export interface Segment {
    id?: number;
    start_time: string;
    end_time: string;
    time_per_appointment: number;
    day: Day;
    overbooking: boolean;
    overbooking_number?: number;
}

export interface PopUpActions {
    onChange(): any;
}

export class SegmentHelper {
    hasSegments(day:number, segments: Array<Segment>) {
        if (!segments) {
            return false;
        }

        for (let segment of segments) {
            if (segment.day == day) {
                return true;
            }
        }

        return false;
    }

    getSegmentsOfDay(day:number, segments: Array<Segment>) {
        if (!segments) {
            return [];
        }

        var _segments: Array<Segment> = [];
        for (let segment of segments) {
            if (segment.day == day) {
                _segments.push(segment);
            }
        }

        return _segments;
    }

    getTimeFormat(time:string) {
        return moment(time, 'h:mm:ss').format('hh:mm A').toString();
    }
}