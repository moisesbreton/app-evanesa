import {Role} from "../constants/role.enum";

export interface User {
    id?: number;
    first_name: string;
    last_name: string;
    user_name: string;
    role_id?: Role;
    membership_id?: number;
}