import { Injectable }       from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';
import { AppService }      from './app.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(private appService: AppService) {}

    canActivate(): boolean {
        if (this.appService.checkLoginUser()) {
            return true;
        }

        this.appService.redirectToURL("/login");

        return false;
    }

    canActivateChild(): boolean {
        return this.canActivate();
    }
}