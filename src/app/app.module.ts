import {NgModule, APP_INITIALIZER} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import {Http, HttpModule} from "@angular/http";
import {routing, appRoutingProviders} from './app.routing';
import {AppComponent} from './app.component';
import {LoginComponent} from "./login/login.component";
import {CaptionService} from "./shared/caption.service";
import {AuthGuard} from "./auth-guard.service";
import {AppService} from "./app.service";
import {MainModule} from "./main/main.module";


export function captionFactory(caption: CaptionService) {
  return () => caption.load();
}

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpModule,
    MainModule
  ],
  declarations: [
    AppComponent,
    LoginComponent
  ],
  providers: [
    appRoutingProviders,
    CaptionService,
    AuthGuard,
    AppService,
    { provide: APP_INITIALIZER,
      useFactory: captionFactory,
      deps: [CaptionService],
      multi: true
    }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }