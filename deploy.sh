#!/bin/sh
set -eux

##
# Setting the destinantion folder
##

APP_DEST=/var/www/app.bretonworks.com
APP_SRC=dist
APP_HOST=ec1.bretonworks.com
APP_HOST_KEY=~/.ssh/jebrt.pem


##
# Preparing the distribution
##

ng build --prod --aot

##
# Doing the sync
##

rsync -az --force --delete --progress -e "ssh -i $APP_HOST_KEY" $APP_SRC/* admin@$APP_HOST:$APP_DEST